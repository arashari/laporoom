const path = require("path");
const express = require("express");
const bodyParser = require("body-parser");
const expressLayouts = require('express-ejs-layouts');

const routes = require("./routes");

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static("public"));
app.engine("html", require("ejs").renderFile);
app.use(expressLayouts);
app.set("view engine", "ejs");
app.set('layout', 'layouts/main');
app.use(routes)

const port = process.env.PORT || 3000; // Port we will listen on
app.listen(port, () => console.log(`This app is listening on port ${port}`));
process.on('SIGTERM', () => {
  console.info('SIGTERM signal received.');
  console.log('Closing http server.');
  server.close(() => {
    console.log('Http server closed.');
  });
});
