const express = require("express");
const router = express.Router();
const service = require("./service");

router.get("/", (req, res) => {
  res.render("index");
});

router.post("/", async (req, res, next) => {
  const { subject, content } = req.body;

  const [err, id] = await service.submitReport({
    subject,
    content
  });

  if (err) {
    return next(err);
  }

  res.render("postreport", { id });
});

router.get("/x/:id", async (req, res, next) => {
  const { id } = req.params;

  const [err, val] = await service.getReport(id);

  if (err) {
    return next(err);
  }

  res.render("report", {
    id,
    createdAt: val.createdAt,
    subject: val.subject,
    content: val.content
  });
});

router.get("/admin", async (req, res) => {
  const data = await service.retrieveAll();
  res.render("admin", { data });
});

router.get("*", (req, res) => {
  res.status(404).render("error", { message: "404 not found" });
});

router.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).render("error", { message: err.message })
})

module.exports = router;
