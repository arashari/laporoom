const level = require("level");
const util = require("./util");
const db = level("db");

const submitReport = async (value) => {
  try {
    const id = util.generateId();
    const _value = util.encode({
      ...value,
      v: 1,
      createdAt: Date.now(),
    });
    await db.put(id, _value);

    return [null, id];
  } catch (err) {
    return [err, null];
  }
};

const getReport = async (key) => {
  try {
    const value = util.decode(await db.get(key));

    return [
      null,
      {
        ...value,
        createdAt: new Date(value.createdAt)
      }
    ];
  } catch (err) {
    return [err, null];
  }
};

const retrieveAll = async () => {
  const data = [];
  for await (const [key, value] of db.iterator()) {
    const val = util.decode(value);
    data.push({ id: key, createdAt: val.createdAt, date: new Date(val.createdAt).toString() })
  }

  return data.sort((a, b) => b.createdAt - a.createdAt);
};

module.exports = {
  submitReport,
  getReport,
  retrieveAll,
};
