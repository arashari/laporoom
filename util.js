const nanoid = require('nanoid')
const generator = nanoid.customAlphabet('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz', 12);

const generateId = () => generator();
const base64encode = (x) => new Buffer.from(x).toString("base64");
const base64decode = (x) => new Buffer.from(x, "base64").toString("ascii");
const encode = (x) => base64encode(JSON.stringify(x))
const decode = (x) => JSON.parse(base64decode(x))

module.exports = {
  generateId,
  base64decode,
  base64encode,
  encode,
  decode
}